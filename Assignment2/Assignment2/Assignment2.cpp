// ConsoleApplication1.cpp : définit le point d'entrée pour l'application console.
//

#include "pch.h"
#include <iostream>
#include <omp.h>
int n = 1000000;
float dx = 1.0f / float(n);
double res = 0;

float f(int i)
{
	return 4.0f / (1.0f + powf((float(i)*dx), 2));
}

void sequentialCalc()
{
	double start = omp_get_wtime();
	for (int i = 0; i < n; i++)
	{
		res = res + (f(i) + f(i + 1))*dx / 2.0f;
	}
	double end = omp_get_wtime();
	std::cout << "processing time for sequential: " << (end - start) * 1000 << " ms" << std::endl;
	std::cout << res << "\n";
}

void parallelCalc()
{
	double start = omp_get_wtime();
	omp_set_num_threads(8);
	//#pragma omp parallel for schedule(dynamic,10) reduction (+:res)
	#pragma omp parallel for reduction(+:res)
	for (int i = 0; i < n; i++)
	{
		res = res + (f(i) + f(i + 1))*dx / 2.0f;
	}
	double end = omp_get_wtime();
	std::cout << "processing time for multi-threading: " << (end - start) * 1000 << " ms" << std::endl;
	std::cout << res;
}
int main()
{
	sequentialCalc();
	res = 0;
	parallelCalc();
}