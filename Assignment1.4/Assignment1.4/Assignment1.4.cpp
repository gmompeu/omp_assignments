#include "pch.h"
#include <iostream>
#include <omp.h>

int const size(3);
int mat[size][size];
int vector[size];
int res[size];



int main()
{
	mat[0][0] = 1;
	mat[0][1] = 2;
	mat[0][2] = 3;
	mat[1][0] = 4;
	mat[1][1] = 5;
	mat[1][2] = 6;
	mat[2][0] = 7;
	mat[2][1] = 8;
	mat[2][2] = 9;
	vector[0] = 1;
	vector[1] = 2;
	vector[2] = 3;

	for (int i = 0; i < size; i++)
	{
		int resi = 0;
		for (int j = 0; j < size; j++)
		{
			resi = resi + mat[i][j] * vector[j];
		}
		res[i] = resi;
	}
	std::cout << res[0] << '\n' << res[1] << '\n' << res[2];
	return 0;
}